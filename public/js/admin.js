(function(RipplePlugin) {

	RipplePlugin.admin = {
		login: function(callback){
			templates.parse('ripple/login', {}, function(html) {
				var dialog = bootbox.dialog({
					message: html,
					title: 'Login',
					size: 'small',
					buttons: {
						cancel: {
							label: 'Cancel',
							className: 'btn-normal',
							callback: function() {
								return true;
							}
						},
						login: {
							label: 'Login',
							className: 'btn-primary',
							callback: function() {
								var form = $('#rippleLoginForm'),
									rippleName = form.find('#rippleName').val(),
									password = form.find('#password').val();

								form.find('.alert').addClass('hidden');

								if (rippleName.length > 0 && password.length > 0) {
									// TODO: show progress
									var VC = new ripple.VaultClient();
									VC.loginAndUnlock(rippleName, password, '34535346', function(err,info){
										if (!err && info){
											var settings = {};
											settings.secret = info.secret;
											settings.rippleName = info.username;
											settings.rippleAddress = info.blob.data.account_id;
											RipplePlugin.settings = settings;

											socket.emit('plugins.ripple.saveUserSettings', {
												settings: {
													ripple_name: settings.rippleName,
													ripple_address: settings.rippleAddress
												}
											});

											dialog.modal("hide");

											callback(settings);
										} else {
											// Error
											form.find('.alert').removeClass('hidden').text('An error occurred: ' + err.message);
										}
									});
								}

								return false;
							}
						}
					}
				})
			});
		}
	}

}(window.RipplePlugin));