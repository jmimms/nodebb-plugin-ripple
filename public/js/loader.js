(function(window) {

	$(document).ready(function() {
		RipplePlugin.init();
	});

	window.RipplePlugin = {
		init: function() {
			var self = this;

			socket.emit('plugins.ripple.getConfig', null, function(err, config) {
				var server = config.server && config.server.address ? [config.server.address] : [ 'wss://s1.ripple.com:443' ];

				self.remote = new ripple.Remote({
					servers: server
				});

				self.config = config;

				bindEvents();
			});
		},
		config: {}
	};

	function bindEvents() {
		var tipSelector = '[data-func="ripple.tip"]';
		$(document.body).off('click.ripple', tipSelector).on('click.ripple', tipSelector, handleTipEvent);
	}

	function handleTipEvent(e) {
		var target = $(e.currentTarget),
			address = target.data('address'),
			pid = target.parents('[data-pid]').data('pid');

		showTipPrompt(function(value) {
			RipplePlugin.Tip.perform(address, pid, {
				amount: value,
				currency: RipplePlugin.config.currency,
				issuer: RipplePlugin.config.admin.address
			}, function(err, result) {});
		});
	}

	function getUserSettings(callback) {
		socket.emit('plugins.ripple.getUserSettings', null, function(err, settings) {
			if (settings && settings.settings) {
				callback(null, settings.settings);
			} else {
				callback(err);
			}
		});
	}

	function showTipPrompt(callback) {
		bootbox.prompt('Enter tip amount', function(result) {
			var value = parseInt(result, 10);

			if (isNaN(value)) {
				app.alertError('Invalid tip amount entered');
			} else {
				callback(value);
			}
		});
	}

})(window);