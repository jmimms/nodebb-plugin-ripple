(function(RipplePlugin) {

	RipplePlugin.transaction = {};

	var Tip = {};

	Tip.perform = function(address, pid, transaction, callback) {
		if (!RipplePlugin.settings) {
			RipplePlugin.admin.login(function(settings) {
				doTip(address, pid, transaction, callback);
			});
		} else {
			doTip(address, pid, transaction, callback);
		}
	};

	function doTip(address, pid, transaction, callback) {
		RipplePlugin.remote.setSecret(RipplePlugin.settings.rippleAddress, RipplePlugin.settings.secret);

		var rippleTransaction = RipplePlugin.remote.createTransaction('Payment', {
			account: RipplePlugin.settings.rippleAddress,
			destination: address,
			amount: ripple.Amount.from_human(transaction.amount + transaction.currency + transaction.issuer)
		});

		// TODO : for field reference id of post
		// pid is now available

		rippleTransaction.addMemo('tip', JSON.stringify({domain : window.location.hostname}));

		rippleTransaction.submit(function(err, res) {
			if (err) {
				app.alertError('An error occurred with your tip');
			} else {
				app.alertSuccess('Successfully tipped');
			}
		});
	}

	RipplePlugin.Tip = Tip;

})(window.RipplePlugin);