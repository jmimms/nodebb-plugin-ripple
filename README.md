# NodeBB Ripple


nodebb-plugin-ripple builds on psychobunny's nodebb-plugin-cash, integrating Schamper's unique architecture with a new vision for forum rewards.

Allows for distribution of a Ripple currency customized and issued by the forum owner.

Features include reddit style post ratings, wallet connect in user profile, wallet connect for forum administrator, tip buttons, and leaderboard.

Options for administration include forum owner wallet connect, server for forum goers to connect to rippled, and weighting of rating based algorithm rewards.


## Installation

    npm install nodebb-plugin-ripple