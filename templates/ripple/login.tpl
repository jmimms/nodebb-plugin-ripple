<form class="form" id="rippleLoginForm">
    <div class="alert alert-danger hidden" role="alert"></div>
    <div class="form-group">
        <label for="rippleName">Ripple Name</label>
        <input type="text" id="rippleName" class="form-control"/>
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" id="password" class="form-control"/>
    </div>
</form>