<div class="row">
    <form class="form" id="rippleAdminForm">
        <div class="col-xs-6 pull-left">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-money"></i>
                    Ripple
                    <button class="btn btn-success btn-xs pull-right save">Save</button>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="currency">Currency</label>
                        <input type="text" id="currency" class="form-control" maxlength="3" data-key="currency"/>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label for="server">Server</label>
                        <input type="text" id="server" class="form-control" data-key="server.address"/>
                    </div>

                    <div class="form-group">
                        <label for="blob_vault">Blob vault</label>
                        <input type="text" id="blob_vault" class="form-control" data-key="server.blob_vault"/>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label for="adminAddress">Admin Ripple Name</label>
                        <input type="text" id="adminAddress" class="form-control" data-key="admin.ripplename"/>
                    </div>

                    <div class="form-group">
                        <label for="adminKey">Admin Ripple secret key</label>
                        <input type="text" id="server" class="form-control" data-key="admin.key"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-6 pull-left">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Rating rewards
                    <button class="btn btn-success btn-xs pull-right save">Save</button>
                </div>

                <div class="panel-body">
                    <p class="help-block">Leave either input fields empty to remove it upon saving.</p>
                    <div class="col-xs-6 pull-left">
                        <label>Rating</label>
                        <div data-list="rating">

                        </div>
                    </div>
                    <div class="col-xs-6 pull-right">
                        <label>Reward</label>
                        <div data-list="reward">

                        </div>
                    </div>
                    <button data-func="add" class="btn btn-normal">Add</button>
                </div>
            </div>
        </div>

        <input type="text" data-key="rating_rewards" class="hidden"/>
    </form>
</div>

<script>
    require(['settings'], function (settings) {
        var wrapper = $('#rippleAdminForm'),
                tpl = '<div class="form-group"><input type="{type}" class="form-control" value="{value}"/></div>';

        function makeHTML(type, value) {
            return templates.parse(tpl, {
                type: type,
                value: value
            });
        }

        settings.sync('ripple', wrapper, function() {
            var ratings = $('[data-key="rating_rewards"]').val();

            if (ratings.length > 0) {
                var rating_rewards = JSON.parse(ratings);

                var htmlRating = '', htmlReward = '';
                for (var a in rating_rewards) {
                    if (rating_rewards.hasOwnProperty(a)) {
                        htmlRating += makeHTML('number', rating_rewards[a].rating);
                        htmlReward += makeHTML('text', rating_rewards[a].reward);
                    }
                }
                $('[data-list="rating"]').html(htmlRating);
                $('[data-list="reward"]').html(htmlReward);
            }
        });

        $('.save').click(function(event) {
            event.preventDefault();

            var rating_rewards = [], ratings = [], remove = [];
            $('[data-list="rating"] input').each(function(index, el) {
                if (el.value.length > 0){
                    ratings.push(el.value);
                }
                else {
                    remove.push(index);
                    $(el).remove();
                }
            });
            $('[data-list="reward"] input').each(function(index, el) {
                if (el.value.length > 0 && remove.indexOf(index) === -1) {
                    rating_rewards.push({
                        rating: ratings[index],
                        reward: el.value
                    });
                } else {
                    $(el).remove();
                }
            });

            $('[data-key="rating_rewards"]').val(JSON.stringify(rating_rewards));

            settings.persist('ripple', wrapper, function(){
                socket.emit('admin.plugins.ripple.sync');
            });
        });

        $('[data-func="add"]').click(function(event) {
            debugger;
            event.preventDefault();

            $('[data-list="rating"]').append(makeHTML('number', ''));
            $('[data-list="reward"]').append(makeHTML('text', ''));
        });
    });
</script>