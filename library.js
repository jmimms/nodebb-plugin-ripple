(function(Ripple) {

	var	NodeBB = require('./lib/nodebb'),
		SocketAdmin = NodeBB.SocketAdmin,
		SocketPlugins = NodeBB.SocketPlugins,

		Config = require('./lib/config'),
		User = require('./lib/user'),
		Post = require('./lib/post'),
		Sockets = require('./lib/sockets');

	Ripple.init = function(params, callback) {
		function renderAdmin(req, res, next) {
			res.render('admin/plugins/' + Config.plugin.id, {});
		}

		params.router.get('/admin/plugins/' + Config.plugin.id, params.middleware.admin.buildHeader, renderAdmin);
		params.router.get('/api/admin/plugins/' + Config.plugin.id, renderAdmin);

		Sockets.events.getUserSettings = Config.userSockets.getSettings;
		Sockets.events.saveUserSettings = Config.userSockets.saveSettings;
		SocketAdmin[Config.plugin.id] = Config.adminSockets;
		SocketPlugins[Config.plugin.id] = Sockets.events;

		callback();
	};

	Ripple.addAdminNavigation = function(menu, callback) {
		menu.plugins.push({
			route: '/plugins/' + Config.plugin.id,
			icon: Config.plugin.icon,
			name: Config.plugin.name
		});

		callback(null, menu);
	};

	Ripple.post = Post;

	Ripple.user = User;

})(module.exports);