(function(Sockets) {

	var NodeBB = require('./nodebb'),
		User = NodeBB.User,

		Config = require('./config'),
		Events = {};

	Events.getConfig = function(socket, data, callback) {
		if (socket.uid === 0) {
			return callback(new Error('invalid-data'));
		}

		var config = Config.global.get(),
			result = config;

		User.isAdministrator(socket.uid, function(err, isAdmin) {
			if (!isAdmin) {
				delete result.admin.key;
			}

			delete result.rating_rewards;

			callback(null, result);
		});

	};

	Sockets.events = Events;

})(module.exports);