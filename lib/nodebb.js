(function(NodeBB) {
	module.exports = {
		// NodeBB modules
		Settings: NodeBB.require('./settings'),
		User: NodeBB.require('./user'),
		Posts: NodeBB.require('./posts'),
		SocketIndex: NodeBB.require('./socket.io/index'),
		SocketPlugins: NodeBB.require('./socket.io/plugins'),
		SocketAdmin: NodeBB.require('./socket.io/admin').plugins,
		db: NodeBB.require('./database')
	}
})(module.parent.parent);