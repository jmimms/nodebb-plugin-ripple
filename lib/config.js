(function(Config) {

	var packageInfo = require('../package.json'),
		pluginInfo = require('../plugin.json'),
		pluginId = pluginInfo.id.replace('nodebb-plugin-', ''),

		NodeBB = require('./nodebb'),
		Settings = NodeBB.Settings,
		User = NodeBB.User,

		async = require('async');

	var adminDefaults = {
		server: {
			address: '',
			blob_vault: ''
		},
		admin: {
			address: '',
			key: ''
		},
		rating_rewards: ''
	};

	var userDefaults = {
		name: '',
		address: ''
	};

	Config.plugin = {
		name: pluginInfo.name,
		id: pluginId,
		version: packageInfo.version,
		icon: 'fa-money'
	};

	Config.adminSockets = {
		sync: function() {
			Config.global.sync();
		}
	};

	Config.userSockets = {
		getSettings: function(socket, data, callback) {
			if (!socket.uid) {
				return callback(new Error('Not logged in.'));
			}

			Config.user.get({ uid: socket.uid, settings: {} }, callback);
		},
		saveSettings: function(socket, data, callback) {
			if (!socket.uid || !data || !data.settings) {
				return callback(new Error('Invalid data.'));
			}

			data.uid = socket.uid;
			Config.user.save(data, callback);
		}
	};

	Config.global = new Settings(Config.plugin.id, Config.plugin.version, adminDefaults);

	Config.user = {
		get: function(data, callback) {
			if (!data || !data.uid) {
				return callback(new Error('Invalid data'));
			}

			User.getUserFields(data.uid, Object.keys(userDefaults).map(function(k){
				return Config.plugin.id + '_' + k;
			}), function(err, result) {
				if (err) {
					return callback(err);
				}

				if (!data.settings) {
					data.settings = {};
				}

				var friendlyKey;
				for (var key in userDefaults) {
					friendlyKey = Config.plugin.id + '_' + key;
					if (userDefaults.hasOwnProperty(key) && result.hasOwnProperty(friendlyKey)) {
						data.settings[friendlyKey] = result[friendlyKey] || userDefaults[key];
					}
				}

				callback(null, data);
			});
		},
		save: function(data, callback) {
			if (!data || !data.uid || !data.settings) {
				return callback(new Error('Invalid data'));
			}

			async.each(Object.keys(userDefaults), function(key, next) {
				key = Config.plugin.id + '_' + key;
				if (!data.settings[key]) {
					return next();
				}

				User.setUserField(data.uid, key, data.settings[key], next);
			}, function(err) {
				if (typeof(callback) === 'function' && !err) {
					// Return the new config with the callback
					Config.user.get(data, callback);
				}
			});
		}
	};

})(module.exports);