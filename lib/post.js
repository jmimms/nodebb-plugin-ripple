(function(Post) {

	var User = require('./user'),

		NodeBB = require('./nodebb'),
		Posts = NodeBB.Posts,
		db = NodeBB.db,
		async = require('async'),

		epoch = new Date(1970, 1, 1).getTime();

	Post.upvote = function(data) {
		handleVote(data, 'upvote');
	};

	Post.downvote = function(data) {
		handleVote(data, 'downvote');
	};

	Post.unvote = function(data) {
		handleVote(data, 'unvote');
	};

	Post.getPosts = function(data, callback) {
		async.map(data.posts, function(post, next) {
			db.getObjectField('ripple:post:' + post.pid, 'rating', function(err, rating) {
				if (err) {
					return next(err);
				}

				post.rating = rating || 0;
				next(null, post);
			});
		}, function(err, posts) {
			if (err) {
				return callback(err);
			}

			data.posts = posts;
			callback(null, data);
		});
	};

	function handleVote(data, hook) {
		Posts.getPostFields(data.pid, ['votes', 'timestamp', 'uid'], function(err, data) {
			var variableCount = 0, count = data.count;

			if (hook !== 'unvote' && data.current !== 'unvote') {
				variableCount = 2;
			} else {
				variableCount = 1;
			}

			if (hook === 'upvote' || (hook === 'unvote' && data.current === 'downvote')) {
				count += variableCount;
			} else if (hook === 'downvote' || (hook === 'unvote' && data.current === 'upvote')) {
				count -= variableCount;
			}

			db.setObjectField('ripple:post:' + data.pid, 'rating', hot(count, new Date(data.timestamp)));

			User.checkReputation(data.uid);
		});
	}

	function hot(score, date){
		function log10(val){
			return Math.log(val) / Math.LN10;
		}

		function epochSeconds(d){
			return (d.getTime() - epoch) / 1000;
		}

		var order = log10(Math.max(Math.abs(score), 1));
		var sign = score > 0 ? 1 : score < 0 ? -1 : 0;
		var seconds = epochSeconds(date) - 1134028003;
		var product = order + sign * seconds / 45000;

		return Math.round(product*10000000)/10000000;
	}

})(module.exports);