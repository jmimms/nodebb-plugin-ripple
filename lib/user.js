(function(User) {

	var Config = require('./config'),

		NodeBB = require('./nodebb'),
		NBBUser = NodeBB.User,

		request = require('request');

	User.register = {};

	User.register.addField = function(data, callback) {
		data.templateData.regFormEntry.push({
			label: 'Ripple Name',
			html: '<div class="input-group"><span class="input-group-addon" id="ripple-wave">~</span><input class="form-control" type="text" name="ripple_name" id="ripple_name" aria-describedby="ripple-wave"/></div>',
			styleName: 'ripple_name'
		});

		callback(null, data);
	};

	User.addCustomFields = function(fields, callback) {
		callback(null, fields.concat(['ripple_address']));
	};

	User.addSettings = function(settings, callback) {
		settings.push({
			title: 'Ripple',
			content: '<strong>Ripple Name</strong><br><div class="input-group"><span class="input-group-addon" id="ripple-wave">~</span><input class="form-control" type="text" data-property="ripple_name" aria-describedby="ripple-wave"/></div>'
		});
		callback(null, settings);
	};

	User.getSettings = function(data, callback) {
		Config.user.get(data, callback);
	};

	User.saveSettings = function(data, callback) {
		// TODO : if new ripple_name lookup ripple_address with API call and save as ripple_address
		// blob_vault_url (i.e. 'https://id.ripple.com/v1/user/') + rippleName
		// data.address
		getRippleAddress(data.settings.ripple_name, function(err, address) {
			if (address) {
				data.settings.ripple_address = address;
			} else {
				data.settings.ripple_address = '';
			}

			//Still save if there's an error
			Config.user.save(data, callback);
		});
	};

	User.addProfileInfo = function(data, callback) {
		Config.user.get({ uid: data.uid, settings: {} }, function(err, settings) {
			var address = settings.settings.ripple_address;

			if (address && address.length > 0) {
				data.profile.push({
					content: '<button class="btn btn-primary btn-xs ripple-tip" data-func="ripple.tip" ' +
						'data-address="' + (address) + '">Tip</button>'
				});
			}

			callback(err, data);
		});
	};

	User.checkReputation = function(uid) {
		NBBUser.getUserField(uid, 'reputation', function(err, reputation) {
			var rewards = JSON.parse(Config.global.get('rating_rewards'));

			rewards.forEach(function(el) {
				if (reputation === parseInt(el.rating, 10)) {
					// TODO: REWARD HERE, REWARD STRING IN el.reward
				}
			});
		});
	};

	function getRippleAddress(username, callback) {
		var blob_vault = Config.global.get('server.blob_vault') || 'https://id.ripple.com/v1/user/';

		if (blob_vault.length > 0) {
			request(blob_vault + username, function(err, response, body) {
				if (!err && body) {
					callback(null, JSON.parse(body).address);
				} else {
					callback(error);
				}
			});
		}
	}

})(module.exports);